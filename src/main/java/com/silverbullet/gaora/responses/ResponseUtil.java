/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.responses;

/**
 *
 * @author prodigy4440
 */
public class ResponseUtil {

  public static BaseResponse success(String description, Object entity) {
    BaseResponse baseResponse = new BaseResponse(ResponseCodes.REQUEST_SUCCESSFUL, description);
    baseResponse.setData(entity);
    return baseResponse;
  }

  public static BaseResponse respond(Integer rc, String description, Object entity) {
    BaseResponse baseResponse = new BaseResponse(rc, description);
    baseResponse.setData(entity);
    return baseResponse;
  }

  public static BaseResponse badInputParam(String description) {
    BaseResponse baseResponse = new BaseResponse(ResponseCodes.BAD_INPUT_PARAM, description);
    return baseResponse;
  }

  public static BaseResponse userNameOrEmailExist() {
    return new BaseResponse(ResponseCodes.ACCOUNT_ALREADY_EXISTS,
            "User with the supplied user name or email exist");
  }

  public static BaseResponse notLoggedIn() {
    return new BaseResponse(ResponseCodes.USER_NOT_LOGGED_IN,
            "User not logged in, Accesss denied.");
  }

  public static BaseResponse recordNotFound(String description) {
    return new BaseResponse(ResponseCodes.NO_RECORD, description);
  }

}
