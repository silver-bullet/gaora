/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.responses;

import java.io.Serializable;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author prodigy4440
 * @param <E> entities
 */
@XmlRootElement(name = "response")
public class BaseResponse<E> implements Serializable {

  private static final long serialVersionUID = -3427784974967460030L;

  private Status status;

  private E data;

  public BaseResponse() {
    this.status = new Status(ResponseCodes.REQUEST_SUCCESSFUL, "Request Successful");
  }

  public BaseResponse(Integer rc, String description) {
    this.status = new Status(rc, description);
  }

  public BaseResponse(Status status, E entity) {
    this.status = status;
    this.data = entity;
  }

  public BaseResponse(Integer rc, String description, E entity) {
    this.status = new Status(rc, description);
    this.data = entity;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public E getData() {
    return data;
  }

  public void setData(E data) {
    this.data = data;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 17 * hash + Objects.hashCode(this.status);
    hash = 17 * hash + Objects.hashCode(this.data);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final BaseResponse<?> other = (BaseResponse<?>) obj;
    if (!Objects.equals(this.status, other.status)) {
      return false;
    }
    if (!Objects.equals(this.data, other.data)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "BaseResponse{" + "status=" + status + ", entity=" + data + '}';
  }

}
