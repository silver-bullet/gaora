/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.request;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.ws.rs.FormParam;

/**
 *
 * @author prodigy4440
 */
public class LoginBean implements Serializable {

    private static final long serialVersionUID = 1381374323195638817L;

    @NotNull(message = "Username not supplied")
    @FormParam("username")
    private String username;

    @NotNull(message = "Password cannot be empty")
    @FormParam("password")
    private String password;

    public LoginBean() {
    }

    public LoginBean(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
