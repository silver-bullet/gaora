/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.config;

import com.silverbullet.gaora.util.MessageLogger;
import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author prodigy4440
 */
@Provider
public class RequestFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // When HttpMethod comes as OPTIONS, just acknowledge that it accepts...
        if (requestContext.getRequest().getMethod().equals("OPTIONS")) {
            // Just send a OK signal back to the browser
            MessageLogger.info(RequestFilter.class, requestContext.toString());
            requestContext.abortWith(Response.status(Response.Status.OK).build());
        }

    }
}
