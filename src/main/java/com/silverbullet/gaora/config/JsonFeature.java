/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.config;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author prodigy4440
 */
@Provider
public class JsonFeature implements Feature{
  @Override
    public boolean configure(FeatureContext context) {
        context.property("jersey.config.server.disableMoxyJson", true);
        // this is in jersey-media-json-jackson
//        context.register(JacksonFeature.class);

        // or from jackson-jaxrs-json-provider
        context.register(JacksonJsonProvider.class);
        // for JAXB annotation support
        context.register(JacksonJaxbJsonProvider.class);

        return true;
    }
}
