/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.ejb;

import javax.annotation.Resource;
import javax.ejb.Stateless;

/**
 *
 * @author prodigy4440
 */
@Stateless
public class LookUpManager {

    @Resource(lookup = "java:module/ModuleName")
    private String moduleName;

    @Resource(lookup = "java:app/AppName")
    private String appName;

    public String getAppName() {
        return appName;
    }

    public String getModuleName() {
        return moduleName;
    }
}
