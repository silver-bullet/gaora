/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.ejb;

import com.silverbullet.gaora.util.BCrypt;
import java.util.Random;
import javax.ejb.Stateless;

/**
 * A Stateless Bean whose instance can be used in generating password, pin, hashing password 
 * and verifying hash using BCrypt.
 *
 * @author prodigy4440
 */
@Stateless
public class PasswordManager {

  private static final String PASSWORD_STRING
          = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  
  private static final String NUMBERS = "0123456789";

  private static final Random RANDOM = new Random();

  /**
   * Generates a password of specific length.
   *
   * @param length The length of the password to be generated
   * @return The generated password
   */
  public String generatePassword(int length) {
    StringBuilder sb = new StringBuilder(length);
    for (int i = 0; i < length; i++) {
      sb.append(PASSWORD_STRING.charAt(RANDOM.nextInt(PASSWORD_STRING.length())));
    }
    return sb.toString();
  }
  
  public String generatePin(int length) {
    StringBuilder sb = new StringBuilder(length);
    for (int i = 0; i < length; i++) {
      sb.append(NUMBERS.charAt(RANDOM.nextInt(NUMBERS.length())));
    }
    return sb.toString();
  }
  
  

  /**
   * Hashes a password passed in as parameter and return the hash of the password using BCrypt.
   *
   *
   * @param password Password to be hashed
   * @return The has of the password
   */
  public String hashPasword(String password) {
    return BCrypt.hashpw(password, BCrypt.gensalt());
  }

  /**
   * Checks if hashing a password matches the hash been passed.
   * 
   * 
   * @param password The password to be hashed
   * @param passwordHash The hash to be compared against
   * @return True if matches and false otherwise.
   */
  public boolean checkPassword(String password, String passwordHash) {
    return BCrypt.checkpw(password, passwordHash);
  }

}
