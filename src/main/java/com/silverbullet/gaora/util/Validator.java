/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.util;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.validator.routines.BigDecimalValidator;
import org.apache.commons.validator.routines.ByteValidator;
import org.apache.commons.validator.routines.DateValidator;
import org.apache.commons.validator.routines.DomainValidator;
import org.apache.commons.validator.routines.DoubleValidator;
import org.apache.commons.validator.routines.FloatValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.apache.commons.validator.routines.LongValidator;
import org.apache.commons.validator.routines.ShortValidator;
import org.apache.commons.validator.routines.UrlValidator;

/**
 *
 * @author prodigy4440
 */
public class Validator {

    /**
     * isEmailValid: Validate email address using Java reg ex. This method
     * checks if the input string is a valid email address.
     *
     * @param email String. Email address to validate
     * @return boolean: true if email address is valid, false otherwise.
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        /* 
         Email format: A valid email address will have following format: 
         [\\w\\.-]+: Begins with word characters, (may include periods and hypens). 
         @: It must have a '@' symbol after initial characters. 
         ([\\w\\-]+\\.)+: '@' must follow by more alphanumeric characters (may include hypens.). 
         This part must also have a "." to separate domain and subdomain names. 
         [A-Z]{2,4}$ : Must end with two to four alaphabets. 
         (This will allow domain names with 2, 3 and 4 characters e.g pa, com, net, wxyz) 
 
         Examples: Following email addresses will pass validation 
         abc@xyz.net; ab.c@tx.gov 
         */
//Initialize reg ex for email.  
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
//Make the comparison case-insensitive.  
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean isPhoneNumberValid(String phoneNumber) {
        if (!phoneNumber.startsWith("+")) {
            phoneNumber = "+" + phoneNumber;
        }
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber phonenumber = phoneNumberUtil.parse(phoneNumber, null);
            return phoneNumberUtil.isValidNumber(phonenumber);
        } catch (NumberParseException ex) {
            return false;
        }
    }

    public static boolean isURLValid(String url) {
        boolean preresponse = DomainValidator.getInstance().isValid(url);
        if (!preresponse) {
            return UrlValidator.getInstance().isValid(url);
        } else {
            return preresponse;
        }
    }

    public static boolean isDateValid(String date) {
        Date validate = DateValidator.getInstance().validate(date);
        return !Objects.isNull(validate);
    }

    public static boolean isTimeValid(String time) {
        if (time.length() != 8) {
            return false;
        }

        if ((time.charAt(2) != ':') | (time.charAt(5) != ':')) {
            return false;
        }
        String hour = time.substring(0, 2);
        try {
            int intHour = Integer.parseInt(hour);
            if(intHour< 0 | intHour >23){
                return false;
            }
        } catch (NumberFormatException nfe) {
            return false;
        }
        String minute = time.substring(3, 5);
        try {
            int intMinute = Integer.parseInt(minute);
             if(intMinute< 0 | intMinute >59){
                return false;
            }
        } catch (NumberFormatException nfe) {
            return false;
        }
        String seconds = time.substring(6, 8);
        try {
            int intSeconds = Integer.parseInt(seconds);
             if(intSeconds< 0 | intSeconds >59){
                return false;
            }
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
    
    public static boolean isNumberValid(String number){
        
        if(!Objects.isNull(IntegerValidator.getInstance().validate(number))){
            return true;
        }
        
        if(!Objects.isNull(DoubleValidator.getInstance().validate(number))){
            return true;
        }
        
        if(!Objects.isNull(LongValidator.getInstance().validate(number))){
            return true;
        }
        
        if(!Objects.isNull(FloatValidator.getInstance().validate(number))){
            return true;
        }
        
        if(!Objects.isNull(ByteValidator.getInstance().validate(number))){
            return true;
        }
        
        if(!Objects.isNull(ShortValidator.getInstance().validate(number))){
            return true;
        }
        
        return !Objects.isNull(BigDecimalValidator.getInstance().validate(number));
    }

}
