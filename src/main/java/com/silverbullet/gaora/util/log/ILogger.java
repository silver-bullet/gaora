/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.util.log;

import java.util.Date;

/**
 *
 * @author prodigy4440
 */
public interface ILogger {

  public void log(String message);

  public void log(String message, Date date);

  public void log(String message, Level level);

  public void log(String message, Date date, Level level);

}
