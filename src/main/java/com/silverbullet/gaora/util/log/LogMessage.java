/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.util.log;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author prodigy4440
 */
@Entity
@Table(name = "log")
public class LogMessage implements Serializable {

  private static final long serialVersionUID = -3620811231707975675L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "log_id", nullable = false, unique = true, updatable = true)
  private Long logId;

  @Column(name = "message",columnDefinition = "LONGTEXT")
  private String message;

  @Column(name = "log_date")
  @Temporal(TemporalType.TIMESTAMP)
  private Date date;

  @Enumerated(EnumType.STRING)
  @Column(name = "log_level")
  private Level level;

  public LogMessage() {
  }

  public LogMessage(String message, Date date, Level level) {
    this.message = message;
    this.date = date;
    this.level = level;
  }

  public Long getLogId() {
    return logId;
  }

  public void setLogId(Long logId) {
    this.logId = logId;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Level getLevel() {
    return level;
  }

  public void setLevel(Level level) {
    this.level = level;
  }

}
