/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.util.log;

import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author prodigy4440
 */
@Stateless
public class DatabaseLogger extends AbstractLogger {

  @PersistenceContext
  private EntityManager em;

  @Override
  public void log(String message, Date date, Level level) {
    LogMessage logMessage = new LogMessage(message, date, level);
    em.persist(logMessage);
  }

}
