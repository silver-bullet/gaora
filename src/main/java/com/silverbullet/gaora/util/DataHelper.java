package com.silverbullet.gaora.util;

import com.google.gson.Gson;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.enterprise.context.ApplicationScoped;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CDI-friendly utility class for input validation.
 *
 * @author Oladeji
 */
@ApplicationScoped
public class DataHelper {

  private static final Logger L = LoggerFactory.getLogger(DataHelper.class);

  private static final EmailValidator EMAIL_VALIDATOR = EmailValidator.getInstance();
  private static final Map<Class, JAXBContext> jaxbContextMap = new HashMap<>();


  /**
   * Meant for inspecting input coming directly from the wild for possible malicious intent.
   * @param inputs inputs to be check
   * @return status of the inputs
   */
  public static boolean areClean(String... inputs) {
    // areClean input
    for (String input : inputs) {
      // Implement checks.
    }
    return true;
  }

  /**
   * Inspects input for possible null references, or emptiness (if an element is a {@link String}). Because we are
   * dealing primarily with SOAP, a {@link String} "{@code ?}" is also considered empty. Empty {@link Collection}s and
   * {@link Map}s are also considered specially.
   * @param inputs inputs to be checked for null or empty
   * @return status of the input
   */
  public static boolean hasBlank(Object... inputs) {
    for (Object obj : inputs) {
      if (obj == null) {
        return true;
      }
      if (obj instanceof String) {
        if (((String) obj).isEmpty() || "?".equals(obj)) {
          return true;
        }
      }
      if (obj instanceof Collection) {
        if (((Collection) obj).isEmpty()) {
          return true;
        }
      }
      if (obj instanceof Map) {
        if (((Map) obj).isEmpty()) {
          return true;
        }
      }
      // Arrays are difficult to deal with so I'm not performing a special check on them.
    }

    return false;
  }

  public static boolean isEmailValid(String email) {
    return hasBlank(email) ? false : EMAIL_VALIDATOR.isValid(email);
  }

  public static String sanitize(String input) {
    if ("?".equals(input)) {
      return null;
    }

    return input;
  }

  public static Date parseDate(XMLGregorianCalendar xmlCalendar) {
    if (xmlCalendar == null) {
      return null;
    }
    return xmlCalendar.toGregorianCalendar().getTime();
  }

  public static <T> String marshalXml(T object) {
    try {
      JAXBContext jaxbContext = jaxbContextMap.get(object.getClass());
      if (jaxbContext == null) {
        jaxbContext = JAXBContext.newInstance(object.getClass());
        jaxbContextMap.put(object.getClass(), jaxbContext);
      }

      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      jaxbMarshaller.marshal(object, outputStream);
      return outputStream.toString("UTF-8");
    } catch (JAXBException | UnsupportedEncodingException exc) {
      L.error("An error occured while marshalling an object of type: " + object.getClass().getSimpleName(), exc);
      return null;
    }
  }

  public static <T> T unmarshalXml(Class<T> type, String xml) {
    if (xml == null) {
      return null;
    }

    T result = null;
    try {
      JAXBContext jaxbContext = jaxbContextMap.get(type);
      if (jaxbContext == null) {
        jaxbContext = JAXBContext.newInstance(type);
        jaxbContextMap.put(type, jaxbContext);
      }

      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      result = (T) unmarshaller.unmarshal(new ByteArrayInputStream(xml.getBytes("UTF-8")));
    } catch (Exception exc) {
      L.warn("Unable to unmarshal the xml stream. Xml = " + xml, exc);
    }

    return result;
  }

  /**
   * Generates a JSON representation of an object. Does not respect JAXB annotations.
   * @param <T> generic object to be marshaled to json
   * @param object object to be marshaled to json
   * @return The marshaled object
   */
  // TODO(oluwasayo): Refactor to respect JAXB annotations.
  public static <T> String marshalJson(T object) {
    if (object == null) {
      return null;
    }

    try {
      return new Gson().toJson(object);
    } catch (Exception ex) {
      L.error("An error occured while marshalling an object of type: " + object.getClass().getSimpleName(), ex);
      return null;
    }
  }

  public static <T> T unmarshalJson(Class<T> t, String json) {
    if (json == null) {
      return null;
    }

    try {
      return new Gson().fromJson(json, t);
    } catch (Exception exc) {
      L.warn("Unable to unmarshal the json string. Json = " + json, exc);
      return null;
    }
  }

  public static byte[] hexStringToByteArray(String hexString) {
    return DatatypeConverter.parseHexBinary(hexString);
  }

  public static String byteArrayToHexString(byte[] array) {
    return DatatypeConverter.printHexBinary(array);
  }

}
