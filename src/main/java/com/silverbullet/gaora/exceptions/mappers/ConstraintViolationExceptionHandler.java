/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.exceptions.mappers;

import com.silverbullet.gaora.responses.ResponseUtil;
import javax.annotation.Priority;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Priorities;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author prodigy4440
 */
@Provider
@Priority(Priorities.USER)
public class ConstraintViolationExceptionHandler implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException exception) {
        ConstraintViolation constraintViolation = 
            (ConstraintViolation)exception.getConstraintViolations().toArray()[0];
        
        return Response.ok(ResponseUtil.badInputParam(constraintViolation.getMessage()))
                .build();
    }

}
