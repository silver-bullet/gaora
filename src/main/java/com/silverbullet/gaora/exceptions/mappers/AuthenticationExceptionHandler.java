/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.exceptions.mappers;

import com.silverbullet.gaora.exceptions.AuthenticationException;
import com.silverbullet.gaora.responses.ResponseUtil;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author prodigy4440
 */
@Provider
public class AuthenticationExceptionHandler implements 
        ExceptionMapper<AuthenticationException> {

  @Override
  public Response toResponse(AuthenticationException exception) {
    return Response.ok(ResponseUtil.notLoggedIn()).build();
  }

}
