/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.exceptions;

import java.io.IOException;
import java.io.Serializable;

/**
 *
 * @author prodigy4440
 */
public class AuthenticationException extends IOException implements Serializable{
  
  private static final long serialVersionUID = 3564435581647264202L;

  public AuthenticationException() {
    super("User not logged in");
  }

  public AuthenticationException(String message) {
    super(message);
  }

  public AuthenticationException(String message, Throwable cause) {
    super(message, cause);
  }

  public AuthenticationException(Throwable cause) {
    super(cause);
  }


  
}
