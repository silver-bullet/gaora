/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.annotations.validation.constraint;

import com.silverbullet.gaora.annotations.validation.Phone;
import com.silverbullet.gaora.util.Validator;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author prodigy4440
 */
public class PhoneValidator implements ConstraintValidator<Phone, String> {

    private Phone phone;

    @Override
    public void initialize(Phone phone) {
        this.phone = phone;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return false;
        }

        return Validator.isPhoneNumberValid(value);
    }

}
