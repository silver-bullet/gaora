/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.annotations.validation;

import com.silverbullet.gaora.annotations.validation.constraint.PhoneValidator;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 * @author prodigy4440
 */
@Documented
@Constraint(validatedBy = PhoneValidator.class)
@Target({
    ElementType.FIELD,
    ElementType.ANNOTATION_TYPE,
    ElementType.METHOD,
    ElementType.CONSTRUCTOR,
    ElementType.PARAMETER
})
@Retention(RetentionPolicy.RUNTIME)
public @interface Phone {
    
    String message() default "{invalid.phone}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
