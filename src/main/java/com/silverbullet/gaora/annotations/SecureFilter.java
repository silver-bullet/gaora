/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.annotations;

import com.silverbullet.gaora.entities.Auth;
import com.silverbullet.gaora.ejb.CacheManager;
import com.silverbullet.gaora.exceptions.AuthenticationException;
import java.io.IOException;
import javax.annotation.Priority;
import javax.ejb.EJB;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author prodigy4440
 */
@Secure
@Provider
@Priority(Priorities.AUTHENTICATION)
public class SecureFilter implements ContainerRequestFilter {

    @EJB
    private CacheManager<String, Auth> cacheManager;

    @Inject
    @AuthenticatedUser
    Event<String> authenticatedDetailsEvent;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // Get the HTTP Authorization header from the request
        String authorizationHeader
                = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        // Check if the HTTP Authorization header is present and formatted correctly 
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            throw new AuthenticationException("Authorization header must be provided");
        }

        // Extract the token from the HTTP Authorization header
//        String token = authorizationHeader.substring("Bearer".length()).trim();
        String token = authorizationHeader.split("\\s+")[1].trim();
        if (!cacheManager.contains(token)) {
            throw new AuthenticationException("Authorization header must be provided");
        } else {
            authenticatedDetailsEvent.fire(token);
        }

    }

}
