/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.silverbullet.gaora.annotations;

import com.silverbullet.gaora.entities.Auth;
import com.silverbullet.gaora.ejb.CacheManager;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;

/**
 *
 * @author prodigy4440
 */
@RequestScoped
public class AuthenticatedDetailsProducer {

    @EJB
    private CacheManager<String, Auth> cacheManager;

    @Produces
    @Dependent
    @AuthenticatedUser
    private Auth authDetails;

    public void handleAuthenticationEvent(@Observes @AuthenticatedUser String token) {
        this.authDetails = cacheManager.get(token);
    }
}
